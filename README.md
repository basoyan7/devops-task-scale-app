# DevOps task: app scale

There is a springboot application, which uses MySQL database to store user data. There is a compose file to bring the stack up using 2 instance of application.

To start a stack run:

`docker-compose up -d`

To run Apache Benchmark against one of instances:

`ab -n 1000 -c 10 -p apache_benchmark_input_file -T 'application/x-www-form-urlencoded; charset=UTF-8' localhost:8081/demo/add`

This will run post request 1000 times against one of the instances, using 10 concurrent requests:

# Task

* Create a local Kubernetes cluster with 2 worker nodes and Load Balancer to distribute incoming traffic on 2 nodes
* Add necessary Kubernetes objects, which will represent the stack describe in Compose file (1 database instance, 2 application instances)
* Scale application to single instance and find the number of concurrent Apache Benchmark requests which application can handle
* Increase number of concurrent request handling by horizontally scaling application
* Optimize memory and/or other application parameters to further increase performance (Java/Pod memory/cpu)


# How To Reproduce
* Install Minicube
* Run Minicube with 2 workers
* Apply k8s directory manifes files
* Run `ab` benchmark as show above
* The actual result varies of Count of CPUs and Memory capacity.
* Minikube commands are shown in this article  https://minikube.sigs.k8s.io/docs/tutorials/multi_node/